# Projet GéoRetroNews

Le scraping s'effectue en python à l'aide du package `require`. Les données collectés sont sauvegardées dans le dossier `scraping/scraped_data`. Le dossier `scraping/scraped_data/parallel_swap` permet de réaliser du scraping parallel et est utilisé comme une mémoire swap. Même si ce dossier est vide, ne pas le supprimer.

Les analyses des données scrapées sont effectuées dans le dossier `analyse` en langage R ou Julia.

## Campagne de scraping

- campagne **Cities** : pour chaque journal, pour chaque ville, pour chaque année, récupération du nombre d'articles citant la ville. (villes : `Pop_villes_1896.csv`)
- campagne **City to City** : pour chaque journal, pour tous les couples de villes, pour chaque année, récupération du nombre d'articles citant les deux villes. (villes : `Pop_villes_1896_selected.csv`)
- campagne **Tiny City to City** : même campagne que précédemment mais avec un nombre de villes restreint, se limitant aux plus importantes et aux plus significatives en terme de répartition géographique.
- campagne **Typical Week** : Récupération des villes fréquemment cité sur une semaine type.

Remarque : la ville de Sète, orthographié Cette change de nom au cours des années 1920. Cette ville est donc exclue des campagnes de scraping.

## Analyses

### Analyse des données issues de la campagne Cities

- Corrélation avec l'accroissement de la population ?

### Analyse des graphes issus de la campagne City to City

degré : nombre de connexions dont bénéficie une ville

#### Visualisation cartographique

La visualisation cartographique doit prendre un parti pris concernant la période étudiée. Plusieurs intervalles de temps peuvent être envisagés :
- 1900 - 1913 soit 14 ans
- 1914 - 1918 (WWI) soit 5 ans
- 1919 - 1928 soit 10 ans
- 1929 - 1938 (avant WWII) soit 10 ans

Ainsi, nous pouvons présenter 4 cartes distinctes qui aide à la visualisation des changements si ils ont lieu.

- Graphe de liaison entre les villes
  - taille de la représentation des villes proportionnel à leur degré
- distorsion de la carte :
  - plus il y a de connexion, plus la distance est faible

#### Analyse

- cluster de villes ?
- que se passe-t-il sans Paris ?
- tester plusieurs méthodes d'analyse :
  - centralisation
  - degré
  - pajek
  - force des liens faibles

Trois axes d'analyse :
- lien entre représentation et population
- relation deux à deux des villes
  - phénomène de cluster
  - distorsion de la carte
- carte de relations

Avec un critère bien choisi, la visualisation de centralité peut prendre une forme temporelle

### Analyse des données issues de la campagne Typical Week


## Visualisation Shiny :
Le shiny Appli_cartos regroupe les diff�rentes cartes cod�es en R
