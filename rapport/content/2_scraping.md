# Scraping et traitement des données {#chap:scraping}

## Le site RetroNews

Nos données sont exclusivement issues du site RetroNews\ [@noauthor_retronews_nodate], le site de presse de la Bibliothèque Nationale de France (BNF). Ce site donne un accès libre et gratuit à plus de 400 titres de presse publiés entre 1631 et 1940. RetroNews se présente à la fois comme "un espace digital de consultation d’archives, un outil de recherche et un magazine pour tous donnant à découvrir l’histoire par les archives de presse".

![Capture d'écran de la page d'accueil du site RetroNews le 12 février 2019](figures/scraping/retronews_front.png){#fig:retronews width=70%}

Un accès abonné permet d'accéder à des recherches avancées à travers l'ensemble de la presse numérisée. Les paramètres de recherche sont détaillés dans le tableau\ \ref{tab:RNparam} en annexe\ \ref{app:recherche_scraping}.

Pour notre projet, nous utilisons les champs suivants : *Tous ces mots*, *Date de publication -- début*, *Date de publication -- fin*, *Titre de presse*.

## RetroNewsScraping, notre librairie python

### Processus de scraping

L'idée du scraping est de simuler le comportement d'un utilisateur réel un grand nombre de fois afin de récupérer un ensemble conséquent de données.

D'un point de vue technique, la recherche sur RetroNews s'effectue par une requête Ajax\ [@noauthor_ajax_2018] qui diffère de la méthode classique de dialogue entre le navigateur et le serveur. En effet, c'est un script JavaScript qui va envoyer la requête au serveur, recevoir la réponse, puis modifier la page en conséquence (protocole XMLHttpRequest). Le scraping doit donc non pas simuler le comportement d'un utilisateur mais envoyer directement des requêtes Ajax au serveur. Concevoir une méthode qui se branche directement sur le back-end du site RetroNews par Ajax est tout à notre avantage étant donné les lenteurs que présente front-end.

Il s'agit donc de mettre en place un module python permettant de scraper le site RetroNews selon des paramètres faciles à modifier. Ainsi, un tel script a pour but de mener des campagnes de scraping de manière aisée et efficace.

Nous utilisons la librairie python Requests\ [@noauthor_python_2019] qui permet de réaliser des requêtes à un serveur et d'en récupérer ses réponses.

Notre module s'articule en deux classes :

- `RetroNewsScraping` qui permet de se connecter au site (méthode `start_session`) puis de réaliser des requêtes et de retourner le nombre d'articles trouvés (méthode `get_hits`).
- `SearchParam` qui permet de générer les paramètres de recherche au format json comme attendu par la recherche Ajax de RetroNews (méthode `get_json`).

La figure\ \ref{fig:scraping} présente l'interaction entre notre module de scraping et le site RetroNews.

![Processus et interaction entre le site RetroNews et notre module de scraping RetroNewsScraping](figures/scraping/scraping.png){#fig:scraping width=100%}

### Exemple simple

Voici un exemple simple de script permettant de récupérer le nombre d’articles publiés par l'humanité en 1930 mentionnant la ville de Lille. L'emploi des classes `SearchParam` et `RetroNewScraping` permettent de travailler sur des scripts se concentrant uniquement sur les paramètres de recherches et leurs réponses. Il est alors aisé de construire des boucles pour réaliser des campagnes de scraping conséquentes comme présenté au chapitre\ \ref{chap:campagnes}. Une documentation technique de notre module est disponible en annexe\ \ref{app:scraping}.

```python
from RetroNewsScraping import *
import datetime

# search parameters
sp = SearchParam()
start = datetime.date(1930, 1, 1)
sp.allTerms = "Lille"
sp.tfPublicationsOr = ["L'Humanité"]
sp.set_published_date(  datetime.date(1930, 1, 1),
                        datetime.date(1931, 1, 1))

# login parameters
login_name = 'pro@francoisremi.fr'
login_pass = 'PASSWORD'
# retronews scraping process
rns = RetroNewsScraping(login_name, login_pass)
rns.start_session() # connexion
occurences = rns.get_hits(sp) # ajax request
print(occurences)
```

### Campagnes de Scraping réalisées {#chap:campagnes}

#### Sélection des villes et des journaux étudiées {#chap:selection_villes}

Concernant les journaux à étudier, nous nous sommes référé à Gilles Bastin qui nous a indiqué de nous concentrer sur les titres de presse suivant : Le Figaro, Le Temps, L'Humanité, Le Matin, Le Petit Journal, Le Petit Parisien, La Presse.

Il nous a également fournit les villes les plus peuplées en 1896. Cependant, nous avons dû travailler ce jeu de données :

* il manque la population pour certaines villes relativement importante notamment de l'est de la France. Celles-ci sont alors reléguées en fin de liste malgré leur taille (concerne entre autre, Strasbourg).
* la ville de Sète, auparavant écrite Cette a changé d'orthographe dans les années 1920. Cette ville a donc été exclue des analyses.
* la ville de Tours étant homonyme d'un nom commun, son nombre de citation était naturellement incorrect. Cette ville a donc été exclue des analyses.

Enfin, les coordonnées géographiques des villes ont été récupérées sur le site [data.gouv.fr](https://data.gouv.fr).

Les données travaillées sont consignées dans le fichier `data/villes_v2.csv`

#### Présentation des campagnes

Nous avons réalisé deux campagnes de scraping distinctes :

* campagne  *Cities* : pour chaque journal, pour chaque ville, pour chaque année, récupération du nombre d'articles citant la ville.
* campagne *City to City*: pour chaque journal, pour tous les couples de villes, pour chaque année, récupération du nombre d'articles mentionnant les deux villes.

### Scraping parallélisé

Les campagnes de scraping réalisées impliquaient une masse relativement importante de requêtes Ajax. Afin d'obtenir des temps d'extraction moindre, nous avons mis en place un processus parallélisé à l'aide des modules python `joblib` et `multiprocessing`. Ceux-ci permettent très simplement de paralléliser un processus comme dans l'exemple d'architecture suivante :

```python
from joblib import Parallel, delayed
import multiprocessing
import pandas as pd

df = pd.DataFrame() # dataframe definition

def process(index, row):
    # work on dataframe...

num_cores = multiprocessing.cpu_count() # get number of cores
Parallel(n_jobs=num_cores)(delayed(process)(index, row) for index, row in df.iterrows())
```

Cependant, une telle architecture pose des problèmes de mémoire si l'on modifie des variables communes aux processus parallélisés. La fonction `Parallel` présente une option pour partager la mémoire mais celle-ci s'est révélée insatisfaisante. Nous avons donc du développer une solution pour réaliser des scraping parallèles sans avoir de soucis de mémoire lors de l'intégration de la réponse du scraping dans une même dataframe. En effet, sans mesure spécifique, le script enregistrait les résultats de scraping à de mauvais emplacements dans la base de donnée.

Pour s'affranchir de tout soucis d'un point de vue mémoire, la solution a été de construire autant de dataframe qu'il y a de processeurs en jeu dans la parallélisation, de leur attribuer un token spécifique en tirant un nombre aléatoire entre 0 et 10 000, de sauvegarder chaque base de donnée dans le dossier `scraped_data/parallel_swap` et de réaliser une fusion des fichiers lorsque la campagne de scraping est intégralement terminée.

Cette solution s'est avérée suffisamment robuste pour faire face aux problèmes qui peuvent survenir lors d'un scraping : perte de connexion internet ou exclusion temporaire du serveur.

Les scripts ainsi construits pour chaque campagne sont les suivants :

* `scraping/flux_city_to_city.py`
* `scraping/cities.py`

Afin de justifier la parallélisation, nous avons réalisé 4100 requêtes consécutives par la même machine en faisant varier le nombre de processeurs utilisés (tableau\ \ref{tab:temps_requetes}). Le fait que le temps de calcul ne soit pas proportionnel au nombre de processeurs peut être expliqué par la capacité limité du nombre de requêtes simultanées au serveur depuis un unique ordinateur. Néanmoins, dès 2 processeurs, en passant de 10'52" à 6'01", nous réalisons un gain de temps de scraping de 45% ce qui est extrêmement intéressant lorsqu'une campagne comme city to city pour 50 villes nécessite plus de 8h avec un unique processeur.

L'ensemble du scraping de ce projet a été réalisé sur un PC fixe sous Debian équipé de 2 processeurs.

Nombre de cœurs utilisés | Temps de scraping | Gain relatif |
|:-------------:|:----------:|:---------:|
1 | 10'52" | - |
2 | 6'01" | 45% |
3 | 5'14" | 52% |
4 | 4'28" | 59% |

Table: Temps de scraping de 4100 requêtes différentes sur le site RetroNews avec la même machine en employant ou non la parallélisation\ \label{tab:temps_requetes}


## Traitement des données {#chap:traitement}

Les données ont donc été extraites par années pour chaque villes et les résultats liés à Tours et à Sète ont été exclus comme précisé au chapitre\ \ref{chap:selection_villes}.

Face à la masse de données récoltées et leur multiples dimensions (géographique et temporelles), nous avons constitué des périodes temporelles afin d'en faciliter notamment la visualisation cartographiques.

Les périodes sont les suivantes :

-	de 1900 à 1913, du début du siècle à l'aube de la première guerre mondiale (14 années)
-	de 1914 à 1918, première guerre mondiale (5 années)
-	de 1919 à 1928, première période d'entre deux guerres (10 années)
-	de 1929 à 1938, seconde période d'entre deux guerres (10 années)

Bien que les données aient été scrapées jusqu'en 1940, nous avons rejeté les années 1939 et 1940 de part leur caractère anormal. En effet, ces années qui correspondent au début de la seconde guerre mondiale semblent contenir beaucoup d'anomalies que nous pouvons assimiler à des défauts d'archivage.

Les périodes n'étant pas de durées égales et afin de les comparer, nous avons calculés les moyennes des citations lorsque cela était nécessaire.
