# Gestion de projet

Nous avons exclusivement travaillé sur la plateforme de travail collaboratif *gricad-gitlab* fournie par l'UGA qui est une instance académique de GitLab. En effet, le logiciel de versionnement git a réunit pour nous plusieurs avantages :

- grande souplesse dans le travail collaboratif.
- gestion aisée de versionnement
- accès en temps réel pour notre tuteur à l'avancement du projet
- réalisation de l'ensemble des tâches du projets dans le même environnement de travail :
  * RetroNewsScraping (python)
  * cartographie (R shiny)
  * analyse de graphes (python)
  * rédaction du rapport (markdown, pandoc, \LaTeX)

Notre dépôt *gricad-gitlab* : [https://gricad-gitlab.univ-grenoble-alpes.fr/mazyf/georetronews](https://gricad-gitlab.univ-grenoble-alpes.fr/mazyf/georetronews)
