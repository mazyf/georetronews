---
title: "Étude de la diversité géographique dans la presse nationale en France de 1900 à 1940"
subtitle: |
	| Projet Tutoré
	| Master 2 Statistiques et Science des Données
author: [Lucie Croisier, François-Rémi Mazy]
professors: [Gilles Bastin (PACTE), Adeline Leclercq Samson (IM2AG), Pierre Mahe (BioMérieux)]
date: "19 février 2019"
keywords: [M2, SSD, projet, tutoré, BNF, sciences sociales]
header-includes:
    - \usepackage{latexrelease}
    - \usepackage{caption}
    - \usepackage{graphicx}
    - \usepackage{subcaption}
    - \usepackage{chngcntr}
indent: true
titlepage: true
titlepage-color: "b9151b"
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "FFFFFF"
titlepage-rule-height: 1
caption-justification: centering
lang: "fr"
logo: "logo/logougablanc.png"
logo-width: 150
toc: true
toc-own-page: true
bibliography: bib/GeoRetroNews.bib
link-citations: true
reference-section-title: Bibliographie
citation-style: bib/institute-of-mathematical-statistics.csl
mainfont: "computer modern"
title-sf: true
subtitle-sf: true
author-sf: true
date-sf: true
section-sf: false
footer-left: "Lucie Croisier et François-Rémi Mazy"
header-right: ""
header-left: "M2SSD Projet Tutoré 2019"
geometry: bottom=1.5cm, top=1.5cm, left=2cm, right=2cm
caption-justification: raggedright
...


\counterwithin{figure}{section}
