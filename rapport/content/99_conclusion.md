% !TEX encoding = UTF-8 Unicode
\newpage

# Conclusion {.unnumbered}

La BNF possède une grande quantité d'archives numérisées et bien classifiées dans l'ensemble. Il est intéressant d'essayer de valoriser ces données peu exploitées, et de les croiser avec d'autres bases de données, notamment celles de l'INSEE.

La période de 1900 à 1940 est riche en événements avec la survenue des deux guerres mondiales qui ont marquées la France. La presse est une vitrine de ces changements.

Après avoir conçu notre propre module de scraping spécialement configuré pour le site RetroNews, nous avons proposé différentes visualisation des données collectées qui s'exprime dans de multiples dimentsion (géographiques et temporelle). Ainsi, nous avons mis en place une application Shiny qui présente différentes cartes. Nous avons également exploré d'autres formes de visualisations particulièrement adaptées aux graphes et avons proposé un indicateur de la domination de la capitale sur la représentation de la province. L'objectif de tout ces efforts de visualisation étant de permettre aux personnes compétentes d'identifier des faits remarquables sociologiquement.

Nous avons fait le choix dans ce projet de nous intéresser exclusivement à la diversité de représentation géographique. Néanmoins, la base de donnée fournie par RetroNews peut être source de nombreuses études pouvant porter par exemple sur la diversité de représentation des classes sociales ou encore des genres. Enfin, toutes ces études invitent à comparer les résultats obtenus avec des observations de la diversité dans la presse contemporaine.

\newpage
