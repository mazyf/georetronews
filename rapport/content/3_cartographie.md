#Cartographie {#chap:carto}

A partir des données de scraping, nous avons réalisé plusieurs cartes regroupées dans une application interactive shiny. Cette application permet de visualiser et d'interpréter différentes données géographiques.

##Descriptif de l'application

L'application shiny présente trois visualisations possibles des données. L'ensemble de ces résultats est directement accessible sur le serveur fournit par RStudio à l'adresse : [https://croisiel.shinyapps.io/Diversite_geographique_dans_la_presse/](https://croisiel.shinyapps.io/Diversite_geographique_dans_la_presse/)

### Cartographie des occurrences de citation et de la population

La première carte est basée sur la première campagne de scraping (*cities*) qui donne pour une cinquantaine des villes les plus peuplées en France métropolitaine en 1896 le nombre de fois où cette ville est citée (figure\ \ref{fig:carte1}). La carte donne deux informations pour chaque ville : le nombre d'occurrence de la ville dans la presse est donnée par la taille du symbole qui représente la ville et la palette de couleur indique la population de la ville en 1896.

![Capture d'écran de l'application shiny - occurrence de citation et population](figures/carto/accueil.png){#fig:carte1 width=100%}

### Cartographie des occurrences de citation comparées à une représentation idéale

La deuxième carte basée sur les mêmes données compare le nombre d'occurrence des villes dans la presse par rapport à une distribution théorique proportionnelle à la taille de la population (figure\ \ref{fig:carte2}).

![Capture d'écran de l'application shiny - occurence de citation comparé à une représentation théorique proportionnelle à la population](figures/carto/carte2.png){#fig:carte2 width=100%}

### Cartographie des relations de co-citation

Enfin, la troisième carte représente les données de la deuxième campagne de scraping (*city to city*). Cette campagne donne pour chaque couple de villes, le nombre d'articles dans lesquelles ces deux villes sont co-citées. La carte prend la forme d'une carte de flux où la taille du flux entre les deux villes est proportionnel au nombre de cooccurrences de ces dernières (figure\ \ref{fig:carte3}).

![Capture d'écran de l'application shiny - graphe des relations de co-citation](figures/carto/carte3.png){#fig:carte3 width=100%}

##Techniques utilisées

Les données, sauvegardées en csv par le scraping sont analysées sous R et visualisées grâce à Shiny.
La partie « ui » de Shiny est composé de trois groupes de boutons radios qui permettent de choisir le type d'informations que l'on souhaite afficher, le journal ainsi que la période (de 1900 à 1913, de 1914 à 1918, de 1919 à 1928 et de 1930 à 1938 comme défini au chapitre \ref{chap:traitement}) dans lesquels les données ont été extraites.

La partie « server » est divisé en trois sous-programmes permettant d'afficher respectivement les trois cartes décrites précédemment.
L'affichage des cartes se fait grâce à trois packages disponibles sous R ; « sf », « sp » permettent de définir des points comme des objets spatiaux, « cartography » permet d'afficher le fond de carte puis de superposer les objets spatiaux en superposant des couches.
Les coordonnées des villes ont été obtenues grâce au package « rgeoapi » une première fois puis enregistré en local. « rgeoapi » est un package qui permet d'interroger la base « API Geo ». Cette base de données issue d'une collaboration entre Etalab^[Etalab coordonne la politique d’ouverture et de partage des données publiques françaises] , l'INSEE^[Institut national de la statistique et des études économiques. Collecte, produit, analyse et diffuse des informations sur l’économie et la société françaises]  et OpenStreetMap^[Projet collaboratif. Base de données géographiques libre]  permet d'avoir accès aux référentiels géographiques français.

Le système géodésique utilisé par API geo est le WGS 84 (World Geodetic System 1984). Les fonds de carte inclus dans le package cartographie sont exprimés dans un système géodesique similaire mais utilisant l'ellipsoïde de référence IAG GRS 80 qui diffère légèrement de celle de référence dans le système WGS 84. Le package sp permet de convertir des données spatiales d'un système géodésique à l'autre.

##Analyse des cartes obtenues

###Population et nombre d'occurrences

Cette première série de cartes (figure\ \ref{fig:carte1}) permet de mettre plusieurs choses en évidence. On remarque d'abord la baisse importante du nombre d'occurrences des villes dans les articles pendant la première guerre mondiale. Cette baisse concerne tous les journaux et toutes les villes. Après la guerre, la presse met plusieurs années à retrouver le volume d'avant guerre. On remarque également que si dans les données brutes Paris semblait supplanter toutes les autres villes en terme de représentation dans la presse, au prorata de la population ce n'est pas forcément le cas. Des villes beaucoup moins peuplées sont citées un grand nombre de fois. C'est face à ce constat, que la réalisation de la deuxième série de cartes est apparue comme pertinente.

##Comparaison entre la couverture réelle et la couverture théorique au prorata du nombre d'habitants

Dans cette deuxième série de carte (figure\ \ref{fig:carte2}), nous avons simulé une distribution théorique dans laquelle le nombre d'occurrences de chaque ville serait proportionnel au nombre d'habitants de la ville. La différence entre cette distribution théorique et le nombre réelle d'occurrences est représentée sur cette série de carte par un jeu de couleurs. Lorsque le nombre d'occurrences théoriques est supérieur au nombre observée, la différence est représentée par une palette de bleu, lorsque il est inférieur, par une palette de rouge.
On note que pour Paris la différence est négative et est très importante (bien que ce soit un peu moins marqué pour *L'Humanité*). La population de Paris était plus de cinq fois supérieur à la deuxième ville en terme de population, le nombre d'occurrences théoriques est donc très élevé pour Paris, ce qui explique cette différence négative si marquée.
On remarque également que la moitié sud de la France parait sous représentée alors que les villes géographiquement proches de Paris ont tendance à être surreprésentées. Là encore, on observe la même tendance dans tous les journaux et à toutes les périodes.

##Réseaux de villes co-citées
La dernière série de cartes est très différente des deux autres puisque c'est une carte type "carte de flux" (figure\ \ref{fig:carte3}). Les liens entre villes sont proportionnels au nombre de co-citation des deux villes dans les mêmes articles.

##Utilisation de l'application

Cette application va être proposée à la BNF (Bibliothèque Nationale de France) qui est en demande de support valorisant les archives de la presse française.
Il serait intéressant de pouvoir comparer les cartes de l'application, qui concerne le début de XXème siècle, avec la presse actuelle. On a vu notamment que la couverture de la presse au niveau nationale était relativement équilibrée durant la période étudiée. Ce n'est plus forcément le cas aujourd'hui, les grands quotidiens nationaux sont moins distribués en province. Cette hypothèse serait à vérifier avec des données récentes.
