\appendix

# Scraping du site RetroNews {#app:scraping}

Nous avons développé un module python dénommé `RetroNewsScraping` permettant de se connecter sur le site RetroNews, d'effectuer des recherches Ajax et d'en récupérer le nombre d'articles retournés.

Librairies requises :

* requests [@noauthor_python_2019] : permet de réaliser des requêtes HTML-POST sur un serveur
* bs4 (BeautifulSoup) : permet de parser les pages HTML retournées par requests
* json : permet de lire et d'écrire des fichiers json
* datetime : permet de gérer des données temporelles

Le module est constitué de deux classes : `RetroNewsScraping` et `SearchParam`. Nous détaillons ci-dessous les principaux attributs et méthodes de ces classes.

## classe SearchParam

### attributs

* `SearchParam.allTerms` (string) désigne tous les mots qui doivent figurer dans l'article.
* `SearchParam.someTerms` (string) désigne les mots dont au moins un doit figurer dans l'article.
* `SearchParam.noneTerms` (string) désigne les mots qui ne doivent pas figurer dans l'article.
* `SearchParam.publishedStart` (datetime) désigne le début de la période temporelle à considérer.
* `SearchParam.piblishedEnd` (datetime) désigne la fin de la période temporelle à considérer.
* `SearchParam.tfPublicationsOr` (liste de string) désigne les journaux à analyser. Exemple : `["Le Figaro (1854-)", "Le Temps", "L'Humanité", "Le Matin", "Le Petit Journal", "Le Petit Parisien", "La Presse"]`

### méthodes

* `SearchParam.get_json()` retourne le json attendu par le site RetroNews lors d'une requête Ajax.

## classe RetroNewsScraping

Constructeur :

`RetroNewsScraping.__init__(login_name (string), login_pass (string))`

### méthodes

* `RetroNewScraping.start_session()` réalise la connexion au site RetroNews en utilisant les identifiants de connexion fournis au constructeur.
* `RetroNewScraping.get_hits(sp (SearchParam))` retourne le nombre d'articles retournés suite à la requête paramétrée par l'objet `SearchParam` noté `sp`.

## Paramètres de recherche avancée {#app:recherche_scraping}

Paramètre | Type |
|-------------|-----------|
Tous ces mots | liste de mots |
Certains des mots | liste de mots |
Aucun des mots | liste de mots |
Cette expression | phrase |
Recherche exacte | booléen |
Seulement dans les unes | booléen |
Date de publication -- début | date |
Date de publication -- fin | date |
Titre de presse | menu déroulant |
Type de presse | menu déroulant |
Périodicité | menu déroulant |
Thématique | menu déroulant |
Lieu de publication | menu déroulant |

Table: Paramètres de recherche avancée sur RetroNews (nécessite d'être abonné) \label{tab:RNparam}

## Paramètres de requête ajax

Le header d'une requête Ajax sur le site RetroNews est de la forme suivante :

```json
{
	"Host": "pv5web.retronews.fr",
	"User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0",
	"Accept": "application/json, text/javascript, */*; q=0.01",
	"Accept-Language": "fr,en-US;q=0.7,en;q=0.3",
	"Accept-Encoding": "gzip, deflate, br",
	"Referer": "https://www.retronews.fr/search",
	"Content-Type": "application/json",
	"Content-Length": "1146",
	"Origin": "https://www.retronews.fr",
	"Connection": "keep-alive"
}
```
