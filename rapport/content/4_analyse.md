# Analyse des données {#chap:analyse}

## Part relative de la capitale {#chap:part_paris_province}

Suite à la campagne *cities* et en parallèle des visualisations cartographiques, il est également envisageable d'analyser la part relative de Paris par rapport à la province. Il s'agit alors de compter le nombre de citations de Paris et de le diviser par le nombre total de citation des 50 premières villes en terme de population en 1900 (figure\ \ref{fig:part_paris_province}).

Nous observons alors que :

* Paris présente un taux de citation de l'ordre de 10 à 18% quelque soit le journal.
* Les titres de presse présentent des part relative de citation de la capitale similaires mis à part
*L'Humanité* dont le taux est majoritairement supérieur.
* La période de la première guerre mondiale marque une dominance de citation de la capitale pour tous les titres de presse.
* *Le Petit Parisien* présente un taux relatif de citation de la capitale particulièrement élevé à partir de 1934.

![Part relative de citation de Paris par rapport à la citation des 50 premières villes de France en terme de population en 1900 pour différents journaux](figures/cities/part_paris_province.pdf){#fig:part_paris_province width=70%}


## Relations inter-villes

L'une de nos deux campagnes de scraping porte sur les co-citations entre les villes deux à deux. Nous obtenons des matrices triangle indiquant le nombre d'articles mentionnant ces couples de villes pour chaque années.

### Graphes de relation

Avec un tel jeu de données, il est possible de construire des graphes dont chaque liaison entre les villes serait pondéré par le nombre d'articles mentionnant ces deux villes. Le module python `igraph` est particulièrement adapté pour la construction, l'analyse et la visualisation des graphes. Nous construisons donc pour chaque période temporelle (définies au chapitre\ \ref{chap:traitement}) un graphe de relation des 13 premières villes en terme de population ne prenant en compte uniquement que les liaisons dont le poids est supérieur au troisième quartile de tous les poids (figure fig:graphs_paris). En effet, sans cette suppression des liaisons faibles, l'algorithme de visualisation employé (Fruchterman-Reingold) ne permet pas de constater le jeu d'influence entre les villes.

Nous constatons que les grandes villes de province peinent à se hisser au niveau des relations entretenues par la capitale avec chacune d'entre-elles. Il est également remarquable que Le Havre soit au même niveau de relation que des villes plus importantes comme Lyon ou Marseille. Enfin, la dernière période voit le nombre de villes dépourvues de relations supérieures au troisième quartile augmenter passant de une à 5.

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/graphs/10_cities_with_Paris_q3_p1.pdf}
        \caption{1900 -- 1913}
        \label{fig:graphs_paris_p1}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/graphs/10_cities_with_Paris_q3_p2.pdf}
        \caption{1914 -- 1918}
        \label{fig:graphs_paris_p2}
    \end{subfigure}\\
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/graphs/10_cities_with_Paris_q3_p3.pdf}
        \caption{1919 -- 1928}
        \label{fig:graphs_paris_p3}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/graphs/10_cities_with_Paris_q3_p4.pdf}
        \caption{1929 -- 1938}
        \label{fig:graphs_paris_p4}
    \end{subfigure}
    \caption{Graphe de relation deux à deux des 13 premières villes en terme de population en 1900 (uniquement les liaisons de poids supérieur au 3ème quartile, algorithme Fruchterman-Reingold)}
    \label{fig:graphs_paris}
\end{figure}

Nous construisons avec la même méthode des graphes qui excluent la ville de Paris afin d'observer uniquement les relations de co-citation entre villes de province (figure\ \ref{fig:graphs_without_paris}).

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/graphs/10_cities_without_Paris_q3_p1.pdf}
        \caption{1900 -- 1913}
        \label{fig:graphs_without_paris_p1}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/graphs/10_cities_without_Paris_q3_p2.pdf}
        \caption{1914 -- 1918}
        \label{fig:graphs_without_paris_p2}
    \end{subfigure}\\
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/graphs/10_cities_without_Paris_q3_p3.pdf}
        \caption{1919 -- 1928}
        \label{fig:graphs_without_paris_p3}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/graphs/10_cities_without_Paris_q3_p4.pdf}
        \caption{1929 -- 1938}
        \label{fig:graphs_without_paris_p4}
    \end{subfigure}
    \caption{Graphe de relation deux à deux des 12 premières villes, Paris exclu, en terme de population en 1900 (uniquement les liaisons de poids supérieur au 3ème quartile, algorithme Fruchterman-Reingold)}
    \label{fig:graphs_without_paris}
\end{figure}

### Évolution de la relation capitale -- province

De la même manière que dans la campagne *cities* où nous avons observé la part relative de Paris par rapport à la province (chapitre\ \ref{chap:part_paris_province}), il est peut-être pertinent d'observer un indicateur similaire issue des données de co-citations. Plusieurs outils d'étude de centralité sont utilisés classiquement en étude des graphes. Néanmoins, basés principalement sur les chemins et les chaînes de citations, leur trouver une signification concrète vis à vis de la nature de nos données n'est pas immédiat. Nous avons donc préféré mettre en place notre propre indicateur qui est simplement pour chaque ville la somme du nombre d'articles la co-citant avec une autre ville. Il s'agit en quelque sorte d'un degré pondéré. Nous calculons ensuite la part de Paris par rapport à la province selon ce degré pondéré (figure \ref{fig:weighted_degrees_paris}).

Nous observons une nouvelle fois la publication particulière des titres de presse durant la première guerre mondiale. De plus, l'ordre est respecté entre les journaux par rapport aux observations réalisées au chapitre\ \ref{chap:part_paris_province}. Enfin, nous remarquons ici une tendance à la décroissance de la part relative de la capitale, chose qui n'a pas été observé au chapitre\ \ref{chap:part_paris_province}.

![Part relative du degré pondéré de Paris par rapport à la somme des degrés pondérés des 50 premières villes en terme de population en 1900 pour différents journaux](figures/graphs/weighted_degrees_paris.pdf){#fig:weighted_degrees_paris width=70%}
