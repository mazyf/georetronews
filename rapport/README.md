Requis :

- pandoc
- pandoc-citeproc
- pdflatex

Pour compiler le document, utiliser la commande pandoc suivante :

> pandoc content/* -o projet_BNF.pdf -N --template sandvogel --filter pandoc-citeproc --listings
