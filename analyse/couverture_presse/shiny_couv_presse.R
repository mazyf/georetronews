library(shiny)
library(shinyWidgets)
library(cartography)
library(rgeoapi)
library(plyr)
library(sp)
library(sf)
library(stringr)

rm()

# Define UI for application that draws a histogram
ui <- fluidPage(
  
  setBackgroundColor("#708090"),
  
  # Application title
  titlePanel("Comparaison avec le nombre d'occurrences théoriques"),
  
  # Sidebar with a slider input 
  sidebarLayout(
    sidebarPanel(
      selectInput("journal", 
                  label = "Selectionner le journal",
                  choices = c("lefigaro", 
                              "lematin",
                              "lepetitjournal",
                              "letemps",
                              "lhumanite"),
                  selected = "figaro"),
      
      sliderTextInput(inputId = "year",
                      label = "Selectionner l'année" ,
                      choices = c(paste("X",as.character(1900:1940),sep="")),
                      selected = "X1904")
    ),
    
    # Show plot
    mainPanel(
      plotOutput("plot")
    )
  )
)

# Define server logic required to draw 
server <- function(input, output) {
  output$plot <- renderPlot({
    
    #données differences
    data_1896<-read.csv(paste("/Users/luciecroisier/Desktop/georetronews/scraping/scraped_data/cities_pop_1896/",input$journal,".csv",sep=""),sep=",",header=TRUE,encoding='UTF-8')
    data_1896<-data_1896[,c("nom","population",input$year)]
    data_1896<-na.omit(data_1896)
    couv_theo<-data_1896
    couv_theo[,3]<-data_1896$population/sum(data_1896$population)*sum(couv_theo[,3])
    diff<-couv_theo
    diff[,3]=data_1896[,3]-diff[,3]
    colnames(diff)[1]<-"name"
    
    #recup villes
    geo<-ldply(sub(" ","-",diff$name), ComByName)
    geo<-geo[(as.integer(geo$codeDepartement)<900) | geo$codeDepartement=="2A",] #enlever la reunion
    geo$name<-geo$name %>% str_to_upper()
    geo<-subset(geo,geo$population>1300)
    geo<-geo[,c(1,7,8)]
    geo<-unique(geo)
    data_merge<-merge(diff,geo,by="name")
    
    pos<-subset(data_merge,data_merge[,3]>=0)
    neg<-subset(data_merge,data_merge[,3]<0)
    neg[,3]<-abs(neg[,3])

    #Creation des villes
    villes_pos.spdf<-SpatialPointsDataFrame(data = pos[,-c(4,5)],coords=pos[,c(5,4)], proj4string = CRS( "+proj=longlat +datum=WGS84 +ellps=WGS84"))
    villes_pos.spdf <- spTransform(villes_pos.spdf, CRS(" +proj=laea +lat_0=52 +lon_0=10 +x_0=4321000 +y_0=3210000 +ellps=GRS80 +units=m +no_defs "))
    
    villes_neg.spdf<-SpatialPointsDataFrame(data = neg[,-c(4,5)],coords=neg[,c(5,4)], proj4string = CRS( "+proj=longlat +datum=WGS84 +ellps=WGS84"))
    villes_neg.spdf <- spTransform(villes_neg.spdf, CRS(" +proj=laea +lat_0=52 +lon_0=10 +x_0=4321000 +y_0=3210000 +ellps=GRS80 +units=m +no_defs "))
    
    
    #Chargement due la carte
    data("nuts2006")
    fond_carte <- nuts0.spdf[nuts0.spdf$id == "FR", ]# extract France
    
    plot(fond_carte,col="#FFFFCC")
    col_neg=carto.pal(pal1 = "red.pal",n1=7)
    col_pos=carto.pal(pal1 = "blue.pal",n1=7)
    
    propSymbolsChoroLayer(spdf = villes_neg.spdf, df = data_merge,spdfid = "name",dfid = "name",
                          var = input$year,var2=input$year,
                          breaks =c(0,50,500,1500,5000,8000,10000,100000),
                          col=col_neg,fixmax=10000,inches = 0.2,legend.var.pos="bottomleft",
                          legend.var2.pos="bottomright",legend.var2.title.txt="difference \nnegative")
    propSymbolsChoroLayer(spdf = villes_pos.spdf, df = data_merge,spdfid = "name",dfid = "name",
                          var = input$year,var2=input$year,
                          breaks =c(0,50,500,1500,5000,8000,10000,100000),
                          col=col_pos,fixmax=10000,inches=0.2,legend.var.pos="n",
                          legend.var2.pos="topright",legend.var2.title.txt="difference \npositive")
  })
}

# Run the application 
shinyApp(ui = ui, server = server)

