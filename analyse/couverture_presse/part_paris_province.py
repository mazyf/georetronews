import numpy as np
import pandas as pd
import igraph as ig
from tqdm import tqdm
import copy
import statistics as stat
import matplotlib.pyplot as plt

excluded_cities = ['TOURS']
selected_cities = ['LYON','MARSEILLE','BORDEAUX','LILLE','TOULOUSE','SAINT-ETIENNE','ROUBAIX','LE HAVRE','ROUEN','REIMS','NANCY','TOULON','NICE','AMIENS','LIMOGES','ANGERS','NIMES','BREST','MONTPELLIER','STRASBOURG']
selected_cities = ['PARIS','LYON','MARSEILLE','BORDEAUX','LILLE','TOULOUSE','SAINT-ETIENNE','ROUBAIX','LE HAVRE','ROUEN','REIMS','NANCY','STRASBOURG']

years = range(1900,1939)

journaux = ['lematin','lefigaro','letemps','lhumanite','lepetitparisien','lepetitjournal']

for journal in journaux:
    print(journal)
    cities = pd.read_csv("../../scraping/scraped_data/cities_pop_1896/"+journal+".csv", sep=",")

    cities = cities.loc[cities["Unnamed: 0"]<51]
    cities = cities.loc[~cities.nom.isin(excluded_cities)]

    weight = []
    for y in years:
        weight.append(cities.loc[cities.nom=='PARIS'][str(y)]/sum(cities.loc[cities.nom!='PARIS'][str(y)]))

    plt.plot(years, weight, label=journal)
plt.legend()
plt.xlabel("année")
plt.ylabel("part relative du nombre d'articles mentionnant Paris")
plt.savefig("../../rapport/figures/cities/part_paris_province.pdf")
