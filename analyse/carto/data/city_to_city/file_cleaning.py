import pandas as pd

input_file = "lhumanite_0_350.csv"
output_file = "lhumanite.csv"

df = pd.read_csv(input_file, sep=",")
df['id'] = range(0,len(df.A))

df = df.drop('Unnamed: 0', 1)
df = df.drop('Unnamed: 0.1', 1)
df = df.drop('token', 1)

cols = df.columns.tolist()
cols = cols[-1:] + cols[:-1]
df = df[cols]

df.to_csv(output_file, index = False)
