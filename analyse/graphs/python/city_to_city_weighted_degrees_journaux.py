import numpy as np
import pandas as pd
import igraph as ig
from tqdm import tqdm
import copy
import statistics as stat
import matplotlib.pyplot as plt

excluded_cities = ['TOURS']
selected_cities = ['LYON','MARSEILLE','BORDEAUX','LILLE','TOULOUSE','SAINT-ETIENNE','ROUBAIX','LE HAVRE','ROUEN','REIMS','NANCY','TOULON','NICE','AMIENS','LIMOGES','ANGERS','NIMES','BREST','MONTPELLIER','STRASBOURG']
selected_cities = ['PARIS','LYON','MARSEILLE','BORDEAUX','LILLE','TOULOUSE','SAINT-ETIENNE','ROUBAIX','LE HAVRE','ROUEN','REIMS','NANCY','STRASBOURG']

cities = pd.read_csv("../../../data/villes/villes_v2.csv", sep=",")
cities.id = cities.id -1
cities = cities.loc[cities.id<51]
cities = cities.loc[~cities.name.isin(excluded_cities)]

print(cities)
years = range(1900,1939)

journaux = ['lematin','lefigaro','letemps','lhumanite']

for journal in journaux:
    print(journal)
    flux = pd.read_csv("../../../scraping/scraped_data/city_to_city/"+journal+".csv", sep=",")

    degrees = np.zeros(shape=(len(cities)+1,len(years)))

    for id_flux in tqdm(range(len(flux))):
        if flux.A[id_flux] in cities.id and flux.B[id_flux] in cities.id:
            for id_y in range(len(years)):
                degrees[flux.A[id_flux], id_y] = degrees[flux.A[id_flux], id_y] + flux[str(years[id_y])][id_flux]
                degrees[flux.B[id_flux], id_y] = degrees[flux.B[id_flux], id_y] + flux[str(years[id_y])][id_flux]

    weight_paris = []
    for y in range(len(years)):
        weight_paris.append(degrees[0,y] / sum(degrees[:,y]))

    plt.plot(years, weight_paris, label=journal)

plt.legend()
plt.xlabel("année")
plt.ylabel("part relative de Paris de ses degrés pondérés par le poids")
plt.savefig("../../../rapport/figures/graphs/weighted_degrees_paris.pdf")
plt.show()
