import numpy as np
import pandas as pd
import igraph as ig
from tqdm import tqdm
import copy
import statistics as stat
import matplotlib.pyplot as plt

excluded_cities = ['TOURS']
selected_cities = ['LYON','MARSEILLE','BORDEAUX','LILLE','TOULOUSE','SAINT-ETIENNE','ROUBAIX','LE HAVRE','ROUEN','REIMS','NANCY','TOULON','NICE','AMIENS','LIMOGES','ANGERS','NIMES','BREST','MONTPELLIER','STRASBOURG']
selected_cities = ['PARIS','LYON','MARSEILLE','BORDEAUX','LILLE','TOULOUSE','SAINT-ETIENNE','ROUBAIX','LE HAVRE','ROUEN','REIMS','NANCY','STRASBOURG']

trigger_plot = False
output_file = "../../../rapport/figures/graphs/10_cities_without_Paris_q3_p"
output_file = None
layout_style = "fr"

perc_to_keep = 0

cities = pd.read_csv("../../../data/villes/villes_v2.csv", sep=",")
flux = pd.read_csv("../../../scraping/scraped_data/city_to_city/lematin.csv", sep=",")

cities.id = cities.id -1
cities = cities.loc[cities.id<53]
cities = cities.loc[~cities.name.isin(excluded_cities)]


print(cities)
years = range(1900,1939)

print(len(cities))
degrees = np.zeros(shape=(len(cities)+1,len(years)))

for y in years:
    cities['d'+str(y)] = 0

for id_flux in tqdm(range(len(flux))):
    if flux.A[id_flux] in cities.id and flux.B[id_flux] in cities.id:
        for id_y in range(len(years)):
            degrees[flux.A[id_flux], id_y] = degrees[flux.A[id_flux], id_y] + flux[str(years[id_y])][id_flux]
            degrees[flux.B[id_flux], id_y] = degrees[flux.B[id_flux], id_y] + flux[str(years[id_y])][id_flux]

for id_v in [0,1,2,3,4,50]:
    plt.plot(years,degrees[id_v,:], label=cities.name[id_v])
plt.legend()
plt.ylabel("degree")
plt.show()

weight_paris = []
for y in range(len(years)):
    weight_paris.append(degrees[0,y] / sum(degrees[:,y]))

plt.plot(years, weight_paris)
plt.show()
