import numpy as np
import pandas as pd
import igraph as ig
from tqdm import tqdm
import copy
import statistics as stat

excluded_cities = ['PARIS','TOURS']
selected_cities = ['LYON','MARSEILLE','BORDEAUX','LILLE','TOULOUSE','SAINT-ETIENNE','ROUBAIX','LE HAVRE','ROUEN','REIMS','NANCY','TOULON','NICE','AMIENS','LIMOGES','ANGERS','NIMES','BREST','MONTPELLIER','STRASBOURG']
selected_cities = ['PARIS','LYON','MARSEILLE','BORDEAUX','LILLE','TOULOUSE','SAINT-ETIENNE','ROUBAIX','LE HAVRE','ROUEN','REIMS','NANCY','STRASBOURG']

trigger_plot = False
output_file = "../../../rapport/figures/graphs/10_cities_without_Paris_q3_p"
output_file = None
layout_style = "fr"

perc_to_keep = 0

cities = pd.read_csv("../../../data/villes/villes_v2.csv", sep=",")
flux = pd.read_csv("../../../scraping/scraped_data/city_to_city/lematin.csv", sep=",")

cities.id = cities.id -1
cities = cities.loc[cities.id!=53]
cities = cities.loc[cities.name.isin(selected_cities)]


print(cities)

periods = [[1900,1913],[1914,1918],[1919,1928],[1929,1938]]

p_weights = []
for p in periods:
    p_weights.append([])

g = ig.Graph()
for id_city in tqdm(cities.id):
    g.add_vertex(name=cities.loc[cities.id == id_city].name[id_city])
print(g)
for id_flux in tqdm(range(len(flux))):
    if flux.A[id_flux] in cities.id and flux.B[id_flux] in cities.id:
        g.add_edge(cities.loc[cities.id == flux.A[id_flux]].name[flux.A[id_flux]],
                   cities.loc[cities.id == flux.B[id_flux]].name[flux.B[id_flux]])
        for id_p in range(len(periods)):
            p_weights[id_p].append(0)
            for y in range(periods[id_p][0], periods[id_p][1]+1):
                p_weights[id_p][-1] = p_weights[id_p][-1] + flux[str(y)][id_flux]

graphs = []
for id_p in range(len(periods)):
    graphs.append(g.copy())
    graphs[-1].es['weight'] = p_weights[id_p]

for g in graphs:
    id_to_remove = []
    max_weight = max(g.es['weight'])
    id_max_weight = g.es['weight'].index(max_weight)
    perc = np.percentile(np.array(g.es['weight']), perc_to_keep)
    for id_edge in range(0,len(g.es['weight'])):
        if g.es['weight'][id_edge] < perc:
            id_to_remove.append(id_edge)
    g.delete_edges(id_to_remove)

if trigger_plot == True:
    k = 0
    for g in graphs:
        k = k + 1
        layout = g.layout(layout_style)
        visual_style = {}
        visual_style["vertex_size"] = 10
        visual_style["vertex_label"] = g.vs["name"]
        visual_style["vertex_label_dist"] = 2
        max_weight = max(g.es['weight'])
        weight = []
        for es_weight in g.es['weight']:
            weight.append(5*es_weight / max_weight)
        visual_style["edge_width"] = weight
        visual_style["layout"] = layout
        visual_style["bbox"] = (500, 500)
        visual_style["margin"] = 40
        if output_file == None:
            ig.plot(g, **visual_style)
        else:
            ig.plot(g, output_file+str(k)+".pdf", **visual_style)



print(graphs[0].strength(vertices = range(len(graphs[0].vs["name"])),weights=graphs[0].es["weight"]))
# print(graphs[0].linegraph())
