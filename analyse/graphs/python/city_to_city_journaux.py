import numpy as np
import pandas as pd
import igraph as ig
from tqdm import tqdm
import copy
import statistics as stat
import matplotlib.pyplot as plt

excluded_cities = ['TOURS']
selected_cities = ['LYON','MARSEILLE','BORDEAUX','LILLE','TOULOUSE','SAINT-ETIENNE','ROUBAIX','LE HAVRE','ROUEN','REIMS','NANCY','TOULON','NICE','AMIENS','LIMOGES','ANGERS','NIMES','BREST','MONTPELLIER','STRASBOURG']
selected_cities = ['PARIS','LYON','MARSEILLE','BORDEAUX','LILLE','TOULOUSE','SAINT-ETIENNE','ROUBAIX','LE HAVRE','ROUEN','REIMS','NANCY','STRASBOURG']

trigger_plot = False
output_file = "../../../rapport/figures/graphs/10_cities_without_Paris_q3_p"
output_file = None
layout_style = "fr"

perc_to_keep = 0

cities = pd.read_csv("../../../data/villes/villes_v2.csv", sep=",")

cities.id = cities.id -1
cities = cities.loc[cities.id!=53]
cities = cities.loc[~cities.name.isin(excluded_cities)]

print(cities)

journaux = ['lematin','lefigaro','letemps','lhumanite']
id_v = 0

for journal in journaux:
    print('\n'+journal)
    flux = pd.read_csv("../../../scraping/scraped_data/city_to_city/"+journal+".csv", sep=",")

    years = range(1900,1939)

    y_weights = []
    for y in years:
        y_weights.append([])

    g = ig.Graph()
    for id_city in tqdm(cities.id):
        g.add_vertex(name=cities.loc[cities.id == id_city].name[id_city])
    print(g)
    for id_flux in tqdm(range(len(flux))):
        if flux.A[id_flux] in cities.id and flux.B[id_flux] in cities.id:
            g.add_edge(cities.loc[cities.id == flux.A[id_flux]].name[flux.A[id_flux]],
                       cities.loc[cities.id == flux.B[id_flux]].name[flux.B[id_flux]])
            for id_y in range(len(years)):
                y_weights[id_y].append(flux[str(years[id_y])][id_flux])

    graphs = []
    for id_y in range(len(years)):
        graphs.append(g.copy())
        graphs[-1].es['weight'] = y_weights[id_y]

    for g in graphs:
        id_to_remove = []
        max_weight = max(g.es['weight'])
        id_max_weight = g.es['weight'].index(max_weight)
        perc = np.percentile(np.array(g.es['weight']), perc_to_keep)
        for id_edge in range(0,len(g.es['weight'])):
            if g.es['weight'][id_edge] < perc:
                id_to_remove.append(id_edge)
        g.delete_edges(id_to_remove)


    # diversity = []
    # for id_y in range(len(years)):
    #     diversity.append(graphs[id_y].diversity(vertices=id_v,
    #                         weights=graphs[id_y].es["weight"]))
    # plt.plot(range(years[0], years[-1]+1),diversity, label=journal)
    clique = []
    for id_y in range(len(years)):
        clique.append(graphs[id_y].clique_number())
plt.legend()
plt.ylabel("clique number   ")
plt.show()

# if trigger_plot == True:
#     k = 0
#     for g in graphs:
#         k = k + 1
#         layout = g.layout(layout_style)
#         visual_style = {}
#         visual_style["vertex_size"] = 10
#         visual_style["vertex_label"] = g.vs["name"]
#         visual_style["vertex_label_dist"] = 2
#         max_weight = max(g.es['weight'])
#         weight = []
#         for es_weight in g.es['weight']:
#             weight.append(5*es_weight / max_weight)
#         visual_style["edge_width"] = weight
#         visual_style["layout"] = layout
#         visual_style["bbox"] = (500, 500)
#         visual_style["margin"] = 40
#         if output_file == None:
#             ig.plot(g, **visual_style)
#         else:
#             ig.plot(g, output_file+str(k)+".pdf", **visual_style)


# for id_v in [0,1,2,3,4,50]:
#     strength = []
#     for id_y in range(len(years)):
#         strength.append(graphs[id_y].strength(vertices=id_v,
#                             weights=graphs[id_y].es["weight"]))
#     plt.plot(range(years[0], years[-1]+1),strength, label=graphs[0].vs["name"][id_v])
# plt.legend()
# plt.ylabel("strength")
# plt.show()
#
# for id_v in [0,1,2,3,4,50]:
#     diversity = []
#     for id_y in range(len(years)):
#         diversity.append(graphs[id_y].diversity(vertices=id_v,
#                             weights=graphs[id_y].es["weight"]))
#     plt.plot(range(years[0], years[-1]+1),diversity, label=graphs[0].vs["name"][id_v])
# plt.legend()
# plt.ylabel("diversity")
# plt.show()
