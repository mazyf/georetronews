import pandas as pd
import os
from tqdm import tqdm

def parallel_files_merging(parallel_folder, output, observed_column = '1900', y_start=1900, y_end=1940):
    total = pd.DataFrame()
    for filename in os.listdir(os.getcwd()+'/'+parallel_folder):
        print('reading '+filename)
        f = pd.read_csv(parallel_folder+'/'+filename, sep=",")
        if total.empty:
            total = f.copy()
        idx = f[observed_column].isnull()
        for id_row in tqdm(range(len(idx))):
            if idx[id_row] == False:
                for y in range(y_start,y_end+1):
                    total.loc[id_row,str(y)] = f.loc[id_row,str(y)]

    total.to_csv(output)
