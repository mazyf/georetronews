# generic
import datetime
import os
import time
from tqdm import tqdm
from joblib import Parallel, delayed
import multiprocessing
import random
import os
import numpy as np
import pandas as pd

# specific
from RetroNewsScraping import *
from tools import *

#==================================================
# PARAMETERS
#==================================================

# login parameters
login_name = 'pro@francoisremi.fr'
login_pass = 'M2SSDBNF'

# search parameters
sp = SearchParam()
sp.tfPublicationsOr = ["L'Humanité"]
y_start = 1900
y_end = 1940

# scraping parameters
id_start_couples = 0
id_end_couples = 99
saving_modulo = 50
saving_band = 3
parallel_folder = 'scraped_data/parallel_swap'
output_file = "scraped_data/city_to_city/test.csv"

#==================================================
# SCRAPING SCRIPT
#==================================================

# retronews scraping initialization
print("retronews login...")
rns = RetroNewsScraping(login_name, login_pass)
rns.start_session()

# cities couple generation
print("scraping initialization...")
cities = pd.read_csv("../data/villes/Pop_villes_1896_selected.csv", sep=",")
cities['id'] = range(0,len(cities.nom))

couples = []
for i in range(0,len(cities.id)):
    for j in range(0,i):
        if [i, j] not in couples:
            couples.append([i,j])

if id_end_couples == None:
    id_end_couples = len(couples)

flux = pd.DataFrame(couples[id_start_couples:id_end_couples],
                    columns=['A', 'B'])

for y in range(y_start,y_end+1):
    flux[str(y)] = None

flux["token"] = None

print("scraping start...")

def scrapingProcess(index, row):
    if flux.loc[0,"token"] == None:
        flux.loc[0,"token"] = round(random.random()*100000)
    id_A = row['A']
    id_B = row['B']
    sp.allTerms = cities.loc[id_A]['nom']+", "+cities.loc[id_B]['nom']
    # flux = flux.copy()
    for y in range(y_start,y_end+1):
        sp.set_published_date(datetime.date(y, 1, 1), datetime.date(y, 1, 1)+datetime.timedelta(days=366))
        flux.loc[index, str(y)] = rns.get_hits(sp)
    if index%saving_modulo < num_cores*saving_band or len(flux.A)-num_cores*saving_band <= index:
        flux.to_csv(parallel_folder+'/'+str(flux.loc[0,"token"])+'.csv')

os.system('rm '+parallel_folder+'/*')
num_cores = multiprocessing.cpu_count()
Parallel(n_jobs=num_cores)(delayed(scrapingProcess)(index, row) for index, row in tqdm(flux.iterrows(), total=len(flux.A)))

# fusion des fichiers créés
print("parallel files merging...")
parallel_files_merging(  parallel_folder=parallel_folder,
                        output=output_file,
                        y_start = y_start,
                        y_end = y_end)

print("scrapping complete.")
