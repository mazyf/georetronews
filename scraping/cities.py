# generic
import datetime
import os
import time
from tqdm import tqdm
from joblib import Parallel, delayed
import multiprocessing
import random
import numpy as np
import pandas as pd

# specific
from RetroNewsScraping import *
from tools import *

#==================================================
# PARAMETERS
#==================================================

# login parameters
login_name = 'pro@francoisremi.fr'
login_pass = 'M2SSDBNF'

# search parameters
sp = SearchParam()
sp.tfPublicationsOr = ["Le Petit Parisien"]
y_start = 1900
y_end = 1940

# scraping parameters
saving_modulo = 50
saving_band = 3
parallel_folder = 'scraped_data/parallel_swap'
output_file = "scraped_data/cities_pop_1896/lepetitparisien.csv"

#==================================================
# SCRAPING SCRIPT
#==================================================

# retronews scraping initialization
print("retronews login...")
rns = RetroNewsScraping(login_name, login_pass)
rns.start_session()

print("scraping initialization...")
cities = pd.read_csv("../data/villes/Pop_villes_1896.csv", sep=",")
cities['id'] = range(0,len(cities.nom))

for y in range(y_start,y_end+1):
    cities[str(y)] = None

cities["token"] = None

print("scraping start...")

def scrapingProcess(index, row):
    if cities.loc[0,"token"] == None:
        cities.loc[0,"token"] = round(random.random()*100000)

    if index < 200:
        return

    sp.allTerms = row['nom']

    for y in range(y_start,y_end+1):
        sp.set_published_date(datetime.date(y, 1, 1), datetime.date(y, 1, 1)+datetime.timedelta(days=366))
        cities.loc[index, str(y)] = rns.get_hits(sp)
    if index%saving_modulo < num_cores*saving_band or len(cities.id)-num_cores*saving_band <= index:
        cities.to_csv(parallel_folder+'/'+str(cities.loc[0,"token"])+'.csv')

os.system('rm '+parallel_folder+'/*')
num_cores = multiprocessing.cpu_count()
Parallel(n_jobs=num_cores)(delayed(scrapingProcess)(index, row) for index, row in tqdm(cities.iterrows(), total=len(cities.id)))

# for index, row in tqdm(cities.iterrows(), total=len(cities.id)):
#     scrapingProcess(index, row)

# fusion des fichiers créés
print("parallel files merging...")
parallel_files_merging(  parallel_folder=parallel_folder,
                        output=output_file,
                        y_start = y_start,
                        y_end = y_end)

print("scrapping complete.")
