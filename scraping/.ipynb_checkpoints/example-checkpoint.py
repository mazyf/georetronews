from RetroNewsScraping import *
import datetime

import numpy as np
import pandas as pd

# login parameters
login_name = 'pro@francoisremi.fr'
login_pass = 'M2SSDBNF'

# search parameters
sp = SearchParam()


# retronews scraping initialization
rns = RetroNewsScraping(login_name, login_pass)
rns.start_session()


start = datetime.date(1920, 1, 1)

item_occurences = np.array([])
all_publications = np.array([])
for i in range(11):
    sp.set_published_date(start, start+datetime.timedelta(days=366))

    sp.allTerms = "Lille"
    item_occurences = np.append(item_occurences,rns.get_hits(sp))
    sp.allTerms = None
    all_publications = np.append(all_publications,rns.get_hits(sp))

    start = start + datetime.timedelta(days=366)

print(item_occurences/all_publications)
