import requests
from bs4 import BeautifulSoup
import json
from SearchParam import *

class RetroNewsScraping(object):
	def __init__(self, login_name, login_pass):
		self.login_name = login_name
		self.login_pass = login_pass
		self.url_login = "https://www.retronews.fr/user/login"

		with open('ajax_header.json', 'r') as f:
			self.search_json_headers = json.load(f)

	def start_session(self):
		self.session = requests.session()
		page = self.session.get(self.url_login)
		soup = BeautifulSoup(page.text, 'lxml')

		hidden_field = soup.findAll('input', attrs={'name':'form_build_id'})
		form_build_id = hidden_field[2]['value']
		payload = {
			"name": self.login_name,
			"pass": self.login_pass,
			"form_build_id": form_build_id,
			"form_id": 'user_login'
		}

		# post to the login form
		self.session.post(self.url_login, data=payload)

	def get_hits(self, sp):
		r = self.session.post('https://pv5web.retronews.fr/api/search/article-cc',
			data=sp.get_json(),
			headers=self.search_json_headers
		)

		result = r.json()
		hits = result['hits']['total']

		return(hits)
