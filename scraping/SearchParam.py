import json
import datetime

class SearchParam(object):
    """SearchParam produces the POST data for the ajax search request"""
    def __init__(self):
        self.allTerms = "Lille"
        self.publishedStart = "1900-01-01"
        self.publishedEnd = "1940-12-31"
        self.tfPublicationsOr= "" 
        self.someTerms = None
        self.noneTerms = None
        self.phrase = None
        self.exact = False
        self.cover = False


    def set_published_date(self, start, end):
        """ start and end are date objects """
        self.publishedStart = str(start.year)+"-"+str(start.month)+"-"+str(start.day)
        self.publishedEnd = str(end.year)+"-"+str(end.month)+"-"+str(end.day)

    def set_publications(self, publi_list):
        """example of entry : ["Le Figaro (1854-)", "Le Temps", "L'Humanité", "Le Matin", "Le Petit Journal", "Le Petit Parisien", "La Presse"] """
        self.tfPublicationsOr = publi_list

    def set_all_terms(self, s):
        self.allTerms = s

    def set_some_terms(self, s):
        self.someTerms = s

    def get_json(self):
        search_json_data = {
        	"from":0,
        	"size":12,
        	"access":"public",
        	"someTerms":self.someTerms,
        	"allTerms":self.allTerms,
        	"noneTerms":self.noneTerms,
        	"phrase":self.phrase,
        	"exact":self.exact,
        	"cover":self.cover,
        	"highlight":True,
        	"highlightPosition":False,
        	"highlightFragments":4,
        	"highlightFragmentsSize":30,
        	"sort":"score",
        	"useAgg":True,
        	"aggSizes":{
        		"histoPeriods":20,
        		"persons":5,
        		"organizations":5,
        		"geolocs":5,
        		"events":5,
        		"types":5,
        		"periodicities":5,
        		"publications":5,
        		"wikitags":5,
        		"themes":20,
        		"documents":0
        	},
        	"pageNumber":0,
        	"publicationId":0,
        	"documentId":0,
        	"publishedStart":self.publishedStart,
        	"publishedEnd":self.publishedEnd,
        	"publishedMonth":0,
        	"publishedDay":0,
        	"tfThemes":[],
        	"tfThemesOr":[],
        	"tfEvents":[],
        	"tfWikitags":[],
        	"tfHistoPeriods":[],
        	"tfPersons":[],
        	"tfOrganizations":[],
        	"tfTypes":[],
        	"tfTypesOr":[],
        	"tfPeriodicities":[],
        	"tfPeriodicitiesOr":[],
        	"tfGeolocs":[],
        	"tfPublications":[],
        	"tfPublicationsOr":self.tfPublicationsOr,
        	"tfPublicationGeolocs":[],
        	"tfPublicationGeolocsOr":[],
        	"aggPublishedHistogram":None,
        	"useFilter":True,
        	"aggPublishedRanges":[
        		{"from":"1900-01-01","to":"1910-01-01"},
        		{"from":"1910-01-01","to":"1920-01-01"},
        		{"from":"1920-01-01","to":"1930-01-01"},
        		{"from":"1930-01-01","to":"1940-01-01"},
        		{"from":"1940-01-01","to":"1950-01-01"}
        		]
        }
        return(json.dumps(search_json_data))
