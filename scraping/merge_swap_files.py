from tools import *

parallel_folder = 'scraped_data/parallel_swap'
output_file = "scraped_data/city_to_city/lhumanite_950_.csv"
y_start = 1900
y_end = 1940

# fusion des fichiers créés
print("parallel files merging...")
parallel_files_merging(  parallel_folder=parallel_folder,
                        output=output_file,
                        y_start = y_start,
                        y_end = y_end)

print("scrapping complete.")