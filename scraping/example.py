from RetroNewsScraping import *
import datetime

# search parameters
sp = SearchParam()
start = datetime.date(1930, 1, 1)
sp.allTerms = "Lille"
sp.tfPublicationsOr = ["L'Humanité"]
sp.set_published_date(  datetime.date(1930, 1, 1),
                        datetime.date(1931, 1, 1))

# login parameters
login_name = 'pro@francoisremi.fr'
login_pass = 'M2SSDBNF'
# retronews scraping process
rns = RetroNewsScraping(login_name, login_pass)
rns.start_session() # connexion
occurences = rns.get_hits(sp) # ajax request

print(occurences)
